Crunch\Signal
=============

* [List of available packages on packagist.org](http://packagist.org/packages/crunch/signal)

Simple process signal wrapper

Requirements
============
* PHP => 5.4
* ext-pcntl => *

Also ensure, that the `pcntl_*()` are _not_ disabled via `disabled_functions`
`php.ini`-setting.

Usage
=====

Waits ten seconds for a `SIGTERM`-signal.

```php
use Crunch\Signal;

Signal\wait([Signal\TERM], function (Signal\Info $info) {
    // Do something useful
}, 10);
```

Postpone certain signals

```php
use Crunch\Signal;

$oldMask = Signal\block([Signal\TERM, Signal\HUP]);
// Some critical stuff
Signal\reset($oldMask);
```

A simple timer

```php
use Crunch\Signal;

Signal\Timer(5);
Signal\wait([Signal\TERM, Signal\ALRM], function (Signal\Info $info) {
    // Do something useful
}, 10);
```

Register a signal handler. `declare(ticks=x)` is required.

```php
declare(ticks=1);
use Crunch\Signal;

require __DIR__ . '/../vendor/autoload.php';


echo posix_getpid() . PHP_EOL;
Signal\register(Signal\USR1, function (Signal\Info $info) { echo "USR1!"; exit; });

while (true) {
    // very busy code
}
```




Contributors
============
See CONTRIBUTING.md for details on how to contribute.

* Sebastian "KingCrunch" Krebs <krebs.seb@gmail.com> -- http://www.kingcrunch.de/ (german)

License
=======
This library is licensed under the MIT License. See the LICENSE file for details.
