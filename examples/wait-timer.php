<?php
use Crunch\Signal;

require __DIR__ . '/../vendor/autoload.php';

Signal\timer(5);
Signal\wait([Signal\ALRM], function (Signal\Info $info) { var_dump($info); });
