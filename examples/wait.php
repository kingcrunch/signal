<?php
use Crunch\Signal;

require __DIR__ . '/../vendor/autoload.php';

echo posix_getpid() . PHP_EOL;

Signal\wait([Signal\USR1], function (Signal\Info $info) { var_dump($info); }, 1);
