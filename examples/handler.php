<?php
use Crunch\Signal;

require __DIR__ . '/../vendor/autoload.php';


echo posix_getpid() . PHP_EOL;
Signal\register(Signal\USR1, function (Signal\Info $info) { echo "USR1!"; exit; });

sleep(1000);
