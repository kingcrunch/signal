<?php
namespace Crunch\Signal;

class Info
{
    public $number;
    public $error;
    public $code;
    public function __construct ($info)
    {
        $this->number = $info['signo'];
        $this->error = $info['errno'];
        $this->code = $info['code'];
    }
}
