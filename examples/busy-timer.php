<?php
declare(ticks=1);
use Crunch\Signal;

require __DIR__ . '/../vendor/autoload.php';


Signal\timer(3);
Signal\register(Signal\ALRM, function (Signal\Info $info) { echo "ALARM!"; exit; });

while (true) {
    echo ".";
    sleep(1);
}
