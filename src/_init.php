<?php
declare(ticks = 1);
namespace Crunch\Signal;

const HUP = SIGHUP;
const INT = SIGINT;
const QUIT = SIGQUIT;
const ILL = SIGILL;
const TRAP = SIGTRAP;
const ABRT = SIGABRT;
const IOT = SIGIOT;
const BUS = SIGBUS;
const FPE = SIGFPE;
const KILL = SIGKILL;
const USR1 = SIGUSR1;
const SEGV = SIGSEGV;
const USR2 = SIGUSR2;
const PIPE = SIGPIPE;
const ALRM = SIGALRM;
const TERM = SIGTERM;
const STKFLT = SIGSTKFLT;
const CLD = SIGCLD;
const CHLD = SIGCHLD;
const CONT = SIGCONT;
const STOP = SIGSTOP;
const TSTP = SIGTSTP;
const TTIN = SIGTTIN;
const TTOU = SIGTTOU;
const URG = SIGURG;
const XCPU = SIGXCPU;
const XFSZ = SIGXFSZ;
const VTALRM = SIGVTALRM;
const PROF = SIGPROF;
const WINCH = SIGWINCH;
const POLL = SIGPOLL;
const IO = SIGIO;
const PWR = SIGPWR;
const SYS = SIGSYS;
const BABY = SIGBABY;

/**
 * Returns a set of available signals.
 *
 * @return int[]
 */
function signals()
{
    return [HUP, INT, QUIT, ILL, TRAP, ABRT, IOT, BUS, FPE, KILL, USR1, SEGV, USR2, PIPE,
        ALRM, TERM, STKFLT, CLD, CHLD, CONT, STOP, TSTP, TTIN, TTOU, URG, XCPU, XFSZ, VTALRM,
        PROF, WINCH, POLL, IO, PWR, SYS, BABY];
}

function register ($signal, callable $handler)
{
    $handler = function ($number) use ($handler) { return $handler(new Info(['signo' => $number, 'errno' => null, 'code' => null])); };
    if (!\pcntl_signal($signal, $handler)) {
        $error = \pcntl_get_last_error();
        $message = \pcntl_strerror($error);
        throw new SignalException($message, $error);
    }
}

/**
 * Initialize a timer, that triggers an ALRM-signal to _this_ process
 *
 * @param int $seconds
 * @return int Last value
 */
function timer ($seconds)
{
    // TODO "$keepOther"?
    return \pcntl_alarm($seconds);
}

/**
 * Waits until one of the given signals arrived
 *
 * @see block()
 *
 * @param int[]          $signals
 * @param callable       $handler
 * @param int|float|null $timeout Timeout in seconds with fractional part (up to nanoseconds)
 * @return Info
 */
function wait (array $signals, callable $handler = null, $timeout = null)
{
    $info = null;
    $oldBlockMask = block($signals);

    if ($timeout) {
        $seconds = (int) $timeout;
        $nanoSeconds = is_int($timeout) ? 0 : (int) (($timeout - $seconds) * pow(10, 9));
        $signal = \pcntl_sigtimedwait($signals, $info, $seconds, $nanoSeconds);
    } else {
        $signal = \pcntl_sigwaitinfo($signals, $info);
    }

    if (!\pcntl_sigprocmask(SIG_SETMASK, $oldBlockMask)) {
        throw _build_exception();
    }

    if ($signal) {
        throw _build_exception();
    }

    switch ($signal) {
        default:
            $info = new Info($info);
            break;
    }

    if ($handler && $signal > 0) {
        $handler($info);
    }

    return $info;
}

/**
 * Prevents the given signals to do, what they usually do
 *
 * A blocked signal is enqueued as "pending signal" and either catch by {@see wait()},
 * or {@see dispatch()}. This is useful to prevent the system from interrupting
 * critical parts of your application.
 *
 *
 * You are probably looking for {@see wait()}
 *
 * @param int[] $signals
 * @return int[] Old signal map
 */
function block(array $signals)
{
    $old = null;
    if (!\pcntl_sigprocmask(SIG_BLOCK, $signals, $old)) {
        throw _build_exception();
    }

    return $old;
}

/**
 * Unblock previously blocked signals again
 *
 * @param int[] $signals
 * @return int[]
 */
function unblock (array $signals)
{
    $old = null;
    if (!\pcntl_sigprocmask(SIG_UNBLOCK, $signals, $old)) {
        throw _build_exception();
    }

    return $old;
}

/**
 * Removes every block-state except for the signals given
 *
 * @param int[] $exceptSignals
 * @return int[]
 */
function reset (array $exceptSignals = array())
{
    $old = null;
    if (!\pcntl_sigprocmask(SIG_SETMASK, $exceptSignals, $old)) {
        throw _build_exception();
    }

    return $old;
}

function dispatch()
{
    if (!\pcntl_signal_dispatch()) {
        throw _build_exception();
    }
}

/**
 * @internal
 * @return SignalException
 */
function _build_exception()
{
    $error = \pcntl_get_last_error();
    $message = \pcntl_strerror($error);
    return new SignalException($message, $error);
}
